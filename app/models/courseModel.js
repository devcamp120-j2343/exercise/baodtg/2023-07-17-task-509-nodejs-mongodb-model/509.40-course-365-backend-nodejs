//B1: Khai báo thu viện mongoose:
const mongoose = require('mongoose');

//B2: khai báo thu viện Schema của mongoose:
const Schema = mongoose.Schema;

//B3: Tạo đối tượng Schema bao gồm các thuộc tính trong mongoDB:
const courseSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    courseCode: {
        type: String,
        unique: true,
        required: true
    },
    courseName: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    discountPrice: {
        type: Number,
        required: true
    },
    duration: {
        type: String,
        required: true
    },
    level: {
        type: String,
        required: true
    },
    coverImage: {
        type: String,
        required: true
    },
    teacherName: {
        type: String,
        required: true
    },
    teacherPhoto: {
        type: String,
        required: true
    },
    isPopular: {
        type: Boolean,
        default: true
    },
    isTrending: {
        type: Boolean,
        default: false
    }
})

//B4: export schema ra model:
module.exports = mongoose.model('course', courseSchema)
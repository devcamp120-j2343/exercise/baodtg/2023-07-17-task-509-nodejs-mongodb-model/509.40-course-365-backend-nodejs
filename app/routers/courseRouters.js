//Khai báo thư viện express
const express = require('express');
const { getAllCourses, getCourseById, createCourse, updateCourseById, deleteCourseById } = require('../controllers/courseController');

//Tạo router:
const courseRouter = express.Router();

//router get all courses
courseRouter.get("/courses", getAllCourses)

//router get course
courseRouter.get("/courses/:courseId", getCourseById)

//router post courses
courseRouter.post("/courses", createCourse)

//router put course
courseRouter.put("/courses/:courseId", updateCourseById)

//router delete course
courseRouter.delete("/courses/:courseId", deleteCourseById)

module.exports = {courseRouter}
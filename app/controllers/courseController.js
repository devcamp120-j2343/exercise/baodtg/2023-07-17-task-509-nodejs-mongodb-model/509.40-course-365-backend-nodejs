//import course model
const { default: mongoose } = require("mongoose");
const courseModel = require("../models/courseModel");

//Get all courses
const getAllCourses = (req, res) => {
    courseModel.find()
        .then((data) => {
            if (data && data.length > 0) {
                return res.status(200).json({
                    message: `Get all courses successfully`,
                    data
                })
            } else {
                return res.status(404).json({
                    message: `Not found any courses`,
                    data
                })
            }
        })
}

//Get course by Id:
const getCourseById = async (req, res) => {
    const courseId = req.params.courseId
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Course Id is invalid`
        })
    }
    const courseFoundById = await courseModel.findOne({ _id: courseId })
    if (courseFoundById) {
        return res.status(200).json({
            status: `Get course by id ${courseId} succesfully !`,
            data: courseFoundById
        })
    } else {
        return res.status(404).json({
            status: `Not found course by id ${courseId} !`,

        })
    }


}

//Create new course:
const createCourse = (req, res) => {
    //B1: thu thập dữ liệu:
    let { courseCode, courseName, price, discountPrice, duration, level, coverImage, teacherName, teacherPhoto, isPopular, isTrending } = req.body;
    //B2: kiểm tra dữ liệu:
    if (!courseCode) {
        return res.status(400).json({
            message: `courseCode is required`
        })
    }
    if (!courseName) {
        return res.status(400).json({
            message: `courseName is required`
        })
    }
    if (!Number.isInteger(price)) {
        return res.status(400).json({
            message: `price is invalid`
        })
    }
    if (!Number.isInteger(discountPrice)) {
        return res.status(400).json({
            message: `discountPrice is invalid`
        })
    }
    if (!duration) {
        return res.status(400).json({
            message: `duration is required`
        })
    }
    if (!level) {
        return res.status(400).json({
            message: `level is required`
        })
    }
    if (!coverImage) {
        return res.status(400).json({
            message: `coverImage is required`
        })
    }
    if (!teacherName) {
        return res.status(400).json({
            message: `teacherName is required`
        })
    }
    if (!teacherPhoto) {
        return res.status(400).json({
            message: `teacherPhoto is required`
        })
    }
    //B3: thực thi model:
    let createCourseData = new courseModel({
        _id: new mongoose.Types.ObjectId(),
        courseCode,
        courseName,
        price,
        discountPrice,
        duration,
        level,
        coverImage,
        teacherName,
        teacherPhoto,
        isPopular,
        isTrending
    })
    courseModel.create(createCourseData)
        .then((data) => {
            return res.status(201).json({
                status: `Create new course successfully`,
                data
            })
        })
        .catch((error) => {
            return res.status(500).json({
                status: `Internal Server Error`,
                message: error.message
            })
        })
}


//Update course by Id: 
const updateCourseById = async (req, res) => {
    //thu thập dữ liệu
    const courseId = req.params.courseId
    let { courseCode, courseName, price, discountPrice, duration, level, coverImage, teacherName, teacherPhoto, isPopular, isTrending } = req.body;
    //validate
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Course Id is invalid`
        })
    }
    //B2: kiểm tra dữ liệu:
    if (!courseCode) {
        return res.status(400).json({
            message: `courseCode is required`
        })
    }
    if (!courseName) {
        return res.status(400).json({
            message: `courseName is required`
        })
    }
    if (!Number.isInteger(price)) {
        return res.status(400).json({
            message: `price is invalid`
        })
    }
    if (!Number.isInteger(discountPrice)) {
        return res.status(400).json({
            message: `discountPrice is invalid`
        })
    }
    if (!duration) {
        return res.status(400).json({
            message: `duration is required`
        })
    }
    if (!level) {
        return res.status(400).json({
            message: `level is required`
        })
    }
    if (!coverImage) {
        return res.status(400).json({
            message: `coverImage is required`
        })
    }
    if (!teacherName) {
        return res.status(400).json({
            message: `teacherName is required`
        })
    }
    if (!teacherPhoto) {
        return res.status(400).json({
            message: `teacherPhoto is required`
        })
    }
    let updateCourseData = new courseModel({
        courseCode,
        courseName,
        price,
        discountPrice,
        duration,
        level,
        coverImage,
        teacherName,
        teacherPhoto,
        isPopular,
        isTrending
    })
    const courseUpdated = await courseModel.findByIdAndUpdate(courseId, updateCourseData)
    try {
        if (courseUpdated) {
            return res.status(200).json({
                status: `Update course with id ${courseId} successfully`,
                data: courseUpdated
            })
        } else {
            return res.status(200).json({
                status: `Not found any course with id ${courseId}`,
                data: courseUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }

}

//Delete course by Id:
const deleteCourseById = async (req, res) => {
    const courseId = req.params.courseId
    //validate
    if (!mongoose.Types.ObjectId.isValid(courseId)) {
        return res.status(400).json({
            status: `Bad request`,
            message: `Course Id is invalid`
        })
    }
    try {
        const deletedCourse = await courseModel.findByIdAndDelete(courseId)
        if (deletedCourse) {
            return res.status(204).json({
                status: `Delete course with id ${courseId} successfully`,
                data: deletedCourse
            })
        } else {
            return res.status(200).json({
                status: `Not found any course with id ${courseId}`,
                data: courseUpdated
            })
        }
    } catch (error) {
        return res.status(500).json({
            status: `Internal Server Error`,
            message: error.message
        })
    }
}
//export controller:
module.exports = { getAllCourses, getCourseById, createCourse, updateCourseById, deleteCourseById }



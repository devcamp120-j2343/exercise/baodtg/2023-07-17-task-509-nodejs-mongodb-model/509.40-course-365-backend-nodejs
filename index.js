//Khai báo thư viện express
const express = require('express');

//Khai báo model mongoose:
const courseModel = require('./app/models/courseModel')

//khai báo thu vien path
const path = require('path');
const { courseRouter } = require('./app/routers/courseRouters');

//khởi tạo app express
const app = express();
app.use(express.json())

//Khai báo thư viện mongoose
var mongoose = require('mongoose');

//khởi tạo cổng port:
const port = 8000;

//kết nối mongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365")
    .then(() => console.log("Connected to Mongo Successfully"))
    .catch(error => handleError(error))

app.use(express.static(__dirname + "/views"))

app.get("/", (req, res) => {
    console.log(__dirname);
    res.sendFile(path.join(__dirname + "/views/index.html"))
})

app.use("/", courseRouter)

//chạy app:
app.listen(port, () => {
    console.log(`App listen on port ${port}`)
})
module.exports = app
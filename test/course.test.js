const expect = require("chai").expect;
const request = require("supertest");
const courseModel = require("../app/models/courseModel");
const app = require("../index");
const mongoose = require('mongoose');

describe("/courses", () => {
    before(async () => {
        await courseModel.deleteMany({});
    });

    after(async () => {
        mongoose.disconnect();
    });

    it("should connect and disconnect to mongodb", async () => {
        // console.log(mongoose.connection.states);
        mongoose.disconnect();
        mongoose.connection.on('disconnected', () => {
            expect(mongoose.connection.readyState).to.equal(0);
        });
        mongoose.connection.on('connected', () => {
            expect(mongoose.connection.readyState).to.equal(1);
        });
        mongoose.connection.on('error', () => {
            expect(mongoose.connection.readyState).to.equal(99);
        });

        await mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365");
    });

    describe("GET /", () => {
        it("should return all courses", async () => {
            const courses = [
                {  courseCode: "JAVA",
                    courseName: "Lap trinh backend java",
                    price: 300,
                    discountPrice: 270,
                    duration: "3h45m",
                    level: "Beginer",
                    coverImage: "abc",
                    teacherName: "Banh",
                    teacherPhoto: "abc",
                    isPopular: true,
                    isTrending: false },
                {  courseCode: "JAVASCRIPT",
                    courseName: "Lap trinh javascript",
                    price: 300,
                    discountPrice: 270,
                    duration: "3h45m",
                    level: "Beginer",
                    coverImage: "abc",
                    teacherName: "Cong",
                    teacherPhoto: "abc",
                    isPopular: true,
                    isTrending: false },
            ];
            await courseModel.insertMany(courses);
            const res = await request(app).get("/courses");
            expect(res.status).to.equal(200);
            expect(res.body.data.length).to.equal(2);
        });
    });

    describe("GET /:courseId", () => {
        it("should return a course if valid id is passed", async () => {
            const course = {
            _id:new mongoose.Types.ObjectId(),
            courseCode: "DOT NET",
            courseName: "Lap trinh dot net",
            price: 300,
            discountPrice: 270,
            duration: "3h45m",
            level: "Beginer",
            coverImage: "abc",
            teacherName: "Cong",
            teacherPhoto: "abc",
            isPopular: true,
            isTrending: false
        };
            await courseModel.create(course);
            const res = await request(app).get("/courses/" + course._id);
            expect(res.status).to.equal(200);
            expect(res.body.data).to.have.property("courseCode", course.courseCode);
        });

        it("should return 400 error when invalid object id is passed", async () => {
            const res = await request(app).get("/courses/abc");
            expect(res.status).to.equal(400);
        });

        it("should return 404 error when valid object id is passed but does not exist", async () => {
            const res = await request(app).get("/courses/5f43ef20c1d4a133e4628181");
            expect(res.status).to.equal(404);
        });
    });

    describe("POST /", () => {
        it("should return course when the all request body is valid", async () => {
            const res = await request(app)
                .post("/courses")
                .send({ courseCode: "ANDROID",
                courseName: "Lap trinh android",
                price: 300,
                discountPrice: 270,
                duration: "3h45m",
                level: "Beginer",
                coverImage: "abc",
                teacherName: "Cong",
                teacherPhoto: "abc",
                isPopular: true,
                isTrending: false });
            const data = res.body;
            expect(res.status).to.equal(201);
            expect(data.data).to.have.property("_id");
            expect(data.data).to.have.property("courseName", "Lap trinh android");
            expect(data.data).to.have.property("teacherName", "Cong");
            expect(data.data).to.have.property("isPopular", true);

            const course = await courseModel.findOne({ courseCode: 'ANDROID' });
            expect(course.price).to.equal(300);
            expect(course.discountPrice).to.equal(270);
        });
    });

    describe("PUT /:courseId", () => {
        it("should update the existing course and return 200", async () => {
            const course = {
                _id:new mongoose.Types.ObjectId(),
                courseCode: "C++",
                courseName: "Lap trinh C++",
                price: 300,
                discountPrice: 270,
                duration: "3h45m",
                level: "Beginer",
                coverImage: "abc",
                teacherName: "Cong",
                teacherPhoto: "abc",
                isPopular: true,
                isTrending: false
            };
            await courseModel.create(course);

            const res = await request(app)
                .put("/courses/" + course._id)
                .send({ courseCode: "C#",
                courseName: "Lap trinh C#",
                price: 300,
                discountPrice: 270,
                duration: "3h45m",
                level: "Beginer",
                coverImage: "abc",
                teacherName: "Cong",
                teacherPhoto: "abc",
                isPopular: true,
                isTrending: false });

            expect(res.status).to.equal(200);
            const dataAfterUpdate = await courseModel.findById(course._id);
            console.log(dataAfterUpdate)
            expect(dataAfterUpdate).to.have.property("courseCode", "C#");
            expect(dataAfterUpdate).to.have.property("courseName", "Lap trinh C#");
            expect(dataAfterUpdate).to.have.property("isTrending", false);
        });
    });

    let courseId = '';
    describe("DELETE /:courseId", () => {
        it("should delete requested id and return response 200", async () => {
            const course = {
                _id:new mongoose.Types.ObjectId(),
                courseCode: "PYTHON",
                courseName: "Lap trinh python",
                price: 300,
                discountPrice: 270,
                duration: "3h45m",
                level: "Beginer",
                coverImage: "abc",
                teacherName: "Cong",
                teacherPhoto: "abc",
                isPopular: true,
                isTrending: false
            };
            await courseModel.create(course);
            courseId = course._id;
            const res = await request(app).delete("/courses/" + courseId);
            expect(res.status).to.be.equal(204);
        });

        it("should return 404 when deleted course is requested", async () => {
            let res = await request(app).get("/courses/" + courseId);
            expect(res.status).to.be.equal(404);
        });
    });
});